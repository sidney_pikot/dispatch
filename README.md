# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
About the App
```
Messenger like web application. 
```
Ruby version
```
ruby 2.6.3
```
Rails version
```
rails 6.0.0.rc1
```

Tech stacks
```
Postgresql - database
Anycable - webscoket
Haml
Webpacker
Redis
Oauth - google and facebook
```

How to run: 

1. Install dependencies
```
ruby version 2.6.3
rails version 6.0.0.rc1
node version v12.8.1
go version go1.10.4 

install hivemind
```
2. bundle install
3. bundle exec rake db:setup
4. hivemind Procfile.dev
