class AppParticipant < ApplicationRecord
    belongs_to :app_conversation, inverse_of: :app_participants
    belongs_to :user
end
