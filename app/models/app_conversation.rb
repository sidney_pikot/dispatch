class AppConversation < ApplicationRecord
    has_many :app_participants, inverse_of: :app_conversation,dependent: :destroy
    def create_participants users
        users.each do |c|
            self.app_participants.create(user_id: c)
        end
    end
end
