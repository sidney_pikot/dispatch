class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,:omniauthable, omniauth_providers: [:facebook, :google_oauth2]

  has_many :app_participants
  has_many :providers, dependent: :destroy
  before_save :titleize_fullname
  acts_as_messageable

  scope :filtered, -> (filter) {
    order(:fullname).ransack({fullname_cont: filter})
  }


  def self.create_from_provider_data(auth)
    providers.where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.fullname = auth.info.name   # assuming the user model has a name
      user.avatar = auth.info.image # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails, 
      # uncomment the line below to skip the confirmation emails.
      # user.skip_confirmation!
    end
  end
  def online?
    # redis = Redis.new
    # i = !redis.get("user_#{self.id}_online").nil? ? "online" : ""
    # redis.quit
    return "online"

    # !Redis.new.get("user_#{self.id}_online").nil? ? "online" : ""
  end
  def mailboxer_email
    return nil
  end

  def name
    return self.fullname
  end

  def titleize_fullname
    self.fullname.titleize
  end
  
  def conversations
    self.mailbox.conversations
  end
  def convo_with(receiver)
    if [self.id] == receiver
      @conversation = self.conversations.select{|c|(c.becomes(AppConversation).app_participants.map(&:user_id) - [self.id]).empty?}.first
    else
      puts "else"
      @conversation = self.conversations.select{|c| (c.becomes(AppConversation).app_participants.map(&:user_id) != [self.id, self.id]) && (c.becomes(AppConversation).app_participants.map(&:user_id) - receiver - [self.id]).empty? && c.becomes(AppConversation).app_participants.map(&:user_id).length == ([self.id] + receiver).length}.first
    end
  end
end
