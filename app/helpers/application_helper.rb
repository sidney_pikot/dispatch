module ApplicationHelper
    def page_header(text)
        content_for(:page_header) { text.to_s }
    end

    def format_conversation conversation,curren = current_user.id
        conversation = {
            id: conversation.id,
            last_message: conversation.last_message.body,
            participants: conversation.becomes(AppConversation).app_participants.map(&:user).select{|p|p.id != curren}
        }
    end

    def format_message message
        message = {
            id: message.id,
            body: message.body,
            conversation_id:message.conversation_id,
            attachment: message.attachment.file,
            created_at: format_date(message.created_at),
            sender: message.sender_id
        }
    end
    def format_date date
        return date = "Today " + date.strftime("%-l:%M %p") if Time.current.all_day.include?(date)
        return date = "Yesterday " + date.strftime("%-l:%M %p") if (Time.current - 1.day).all_day.include?(date)
        return date = date.strftime("%b %-d, %-l:%M %p") if date.year == Time.current.year
    end
    def is_sender id,curren = current_user.id
        curren == id ? "replies" : "sent" 
    end

    def format_participants participants
        @participants = participants.present? ? participants.map(&:fullname).join(",") : "You"
    end

    def format_avatar participants, current_avatar
        @avatar = participants.present? ? 
                participants.first.avatar.present? ? participants.first.avatar : current_avatar.present? ? current_avatar : "http://emilcarlsson.se/assets/louislitt.png" : "http://emilcarlsson.se/assets/louislitt.png"
    end

    def format_avatar_for_socket participants, current_id
        @avatar = participants.select{|p| p.id != current_id}.present? ? participants.select{|p|p.id != current_id}.first.avatar.present? ? participants.first.avatar : "http://emilcarlsson.se/assets/louislitt.png" : "http://emilcarlsson.se/assets/louislitt.png"
    end
end
