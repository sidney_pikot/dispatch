class Ajax::ConversationsController < ApplicationController
    include ApplicationHelper
    before_action :authenticate_user!
    def home
        result = Services::Conversations::GetConversations.invoke(current_user)
        @conversations = result[:conversations]
        @status = result[:status]
        respond_to :js
    end
    
    def send_message
        result = Services::Conversations::SendMessage.invoke(message_params,current_user)
        # @messages = result[:messages]
        @status = result[:status]
        respond_to :js
    end

    def reply
        result = Services::Conversations::ReplyToConversation.invoke(message_params,current_user)
        @message = result[:message]
        respond_to :js
    end
    def get_conversation
        @message = Mailboxer::Message.new
        result = Services::Conversations::GetMessages.invoke(params[:id],current_user)
        @status = result[:status]
            @convo_id = params[:id]
            @participants = result[:participants].map(&:user).select{|u| u.id != current_user.id}
            @messages = result[:messages]
            puts @messages
        # puts @status
        respond_to :js
    end

    def search_contacts
        @contacts = User.filtered(params[:q]).result
        respond_to :js
    end

    def new_conversation 
        @message = Mailboxer::Message.new
        respond_to :js
    end
    
    def get_contact
        @users = User.where(id: params[:id])
        @conversation = current_user.convo_with(@users.map(&:id))
        if @conversation.present?
            redirect_to :action => 'get_conversation', :id => @conversation.id
        else
            @conversation = @current_user.mailbox.conversations.new
            # @message = @conversation.messages.new
            @participants = @users.select{|u| u.id != current_user.id}
            @messages = @conversation.messages
            respond_to :js
        end
    end
    private 

    def message_params
        params.require(:message).permit(
            :receiver,
            :body,
            :title,
            :id
        )
    end
end
