# frozen_string_literal: true

class Ws::Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  # You should configure your model like this:
  # devise :omniauthable, omniauth_providers: [:twitter]

  # You should also create an action method in this controller like this:
  # def twitter
  # end

  # More info at:
  # https://github.com/plataformatec/devise#omniauth

   # GET|POST /resource/auth/twitter
  # def passthru
  #   super
  # end

  # GET|POST /users/auth/twitter/callback
  # def failure
  #   super
  # end
  def failure
    flash[:error] = 'There was a problem signing you in. Please register or try signing in later.' 
    redirect_to ws_users_sign_in_url
  end
  def facebook
    # @user = User.create_from_provider_data(request.env['omniauth.auth'])
      # if @user.persisted?
      #   sign_in_and_redirect @user
      #   set_flash_message(:notice, :success, kind: 'Facebook') if is_navigational_format?
      # else
      #   flash[:error] = 'There was a problem signing you in through Facebook. Please register or try signing in later.'
      #   redirect_to ws_users_sign_up_url
    # end 
    @user = find_provider(request.env['omniauth.auth'])
    if @user.save
      sign_in_and_redirect @user
      set_flash_message(:notice, :success, kind: 'Facebook') if is_navigational_format?
    else
      flash[:error] = 'There was a problem signing you in through Facebook. Please register or try signing in later.'
      redirect_to ws_users_sign_in_url
    end 
  end
  def google_oauth2
    # You need to implement the method below in your model (e.g. app/models/user.rb)
      # @user = User.from_omniauth(request.env['omniauth.auth'])

      # if @user.persisted?
      #   flash[:notice] = I18n.t 'devise.omniauth_callbacks.success', kind: 'Google'
      #   sign_in_and_redirect @user, event: :authentication
      # else
      #   session['devise.google_data'] = request.env['omniauth.auth'].except(:extra) # Removing extra as it can overflow some session stores
      #   redirect_to new_user_registration_url, alert: @user.errors.full_messages.join("\n")
    # end
    @user = find_provider(request.env['omniauth.auth'])
    if @user.save
      sign_in_and_redirect @user
      set_flash_message(:notice, :success, kind: 'Google') if is_navigational_format?
    else
      flash[:error] = 'There was a problem signing you in through Google. Please register or try signing in later.'
      redirect_to ws_users_sign_in_url
    end 
  end
  
  protected
  def find_provider(auth)
    @user = User.find_by(email: auth.info.email)
    if @user.present?
      providers = @user.providers.where(provider: auth.provider).where(uid: auth.uid)
      @user.avatar = auth.info.image
      @user.fullname = auth.info.name
      @user.providers.create(provider: auth.provider,uid: auth.id) if !providers.present?
      puts auth
      return @user
    else
      @provider = Provider.find_by(provider: auth.provider,uid: auth.uid)
      if @provider.present?
        return @provider.user
      else
        password = Devise.friendly_token[0, 20]
        @user = User.new(email: auth.info.email, password: password, fullname: auth.info.name,avatar: auth.info.image)
        @user.providers.new(provider: auth.provider, uid: auth.uid)
        return @user
      end
    end
  end
  # The path used when OmniAuth fails
  # def after_omniauth_failure_path_for(scope)
  #   super(scope)
  # end
end
