$(document).on('turbolinks:load',function(){
  $.ajax({
    url: "ajax/conversations/home",
    method: 'get',
    cache: false,
    success: function(){
      on_click_search();
      on_click_contact();
      search_listener();
      $('.contact')[0].click();
    },
    error: function(){
      alert("connection problem");
    }
  });

  function on_click_contact(){
    $('ul').on('click','.contact',function(){
      var id = $(this).find(".c_filler_id").html();
      get_message(id);
    })
  }

  function get_message(id){
    $.ajax({
      url: 'ajax/conversations/get_conversation',
      method: 'get',
      data: {
        id: id
      },
      cache: false,
      success: function(){
        toggle_active_contact(id); 
        scroll_to_top();
        reply_message();
      },
      error: function(){
        alert("connection problem");
      }
    })
  }

  function toggle_active_contact(id){
    var contact = $('span:contains("' + id + '")').closest(".contact")
    if(contact["length"] == 1){
      contact.siblings().removeClass("active")
      contact.addClass("active");
    }
  }
  function on_click_search(){
    $('#search').focusin(function(){
      $('.contacts > ul').fadeOut(500);
      search_contact();
      $('.search_list').fadeIn(500);
    }).focusout(function(){
      $('.search_list').fadeOut(300);
      $('.search_input').val("");
      $('.contacts > ul').fadeIn(300);
      setTimeout(function(){
        $('.search_list > ul').empty();
      },500);
    })
  }

  function search_contact(q = ""){
    $.ajax({
      url: 'ajax/conversations/search_contacts',
      method: 'get',
      data: {
        q: q
      },
      success: function(){
        on_click_search_contact();
      }
    });
  }
  function search_listener(){
    $('.search_input').keyup(delay(function(e){
      if($(this).val() != ""){
        q = $(this).val();
        search_contact(q);
      }
    },500))
  }

  function on_click_search_contact(){
    $('.search_contact').on('click',function(){
      var contact_id = $(this).find('input[type="hidden"]').attr('id').substr(3);
      console.log(contact_id);
      $('.contact.active').removeClass('active');
      $.ajax({
        url: 'ajax/conversations/get_contact',
        data: {
          id: contact_id
        },
        method: 'get',
        cache: false,
        success: function(){
          console.log("success");
          scroll_to_top();
          reply_message();
          send_message()
        }
      })
    })
  }

  function reply_message(){
    $('.reply_button').on('click',function(){
      var message = $('#message_body').val();
      var convo_id = $('#convo_id').val();
      App.reply_message.reply_message(message,convo_id);
    })
    
  }
  function send_message(){
    $('.send_button').on('click',function(){
      console.log("pisti")
      var message = $('#message_body').val();
      var receiver_ids = $('#receiver_id').val();
      App.reply_message.send_message(message,receiver_ids);
    })
  }
  
  function scroll_to_top(callback = null){
    var selector = $('.messages');
    if (selector.length){
      selector.animate({scrollTop: (selector.prop("scrollHeight"))},500,callback)
    }
  }
  function delay(fn, ms) {
    let timer = 0
    return function(...args) {
      clearTimeout(timer)
      timer = setTimeout(fn.bind(this, ...args), ms || 0)
    }
  }
})