// Action Cable provides the framework to deal with WebSockets in Rails.
// You can generate new channels where WebSocket features live using the `rails generate channel` command.

import { createConsumer } from "@rails/actioncable"
console.log($('input#id').val())
App = window.App = {}

export default createConsumer('wss://example-app-anycable1.herokuapp.com/cable?id='+$('input#id').val())
// export default createConsumer('ws://localhost:3334/cable?id='+$('input#id').val())

