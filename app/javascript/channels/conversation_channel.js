import consumer from "./consumer"
import {ion} from "ion-sound";

App.reply_message = consumer.subscriptions.create("ConversationChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    
    window.ion.sound({
      sounds: [
          {name: "fart"}
      ],
      path: "",
      preload: true,
      multiplay: true,
      volume: 3.0
    });
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
    console.log("yawa ka")
    if (data['action'] == "reply"){
      reply_to_conversation(data);
    }
    else{
      create_conversation(data);
    }
  },
  reply_message(message,convo_id){
    console.log("huhuhuhu")
    return this.perform('reply_message', {message, convo_id});
  },
  send_message(message,receiver_ids){
    return this.perform('send_message', {message, receiver_ids});
  }
});

function reply_to_conversation(data){
  if (data['status'] == 200){
    if ($(`.list_convo_${data['conversation_id']}`).length){
      $('.messages > ul').append(data['message']);
    }
    $(`.c_filler_${data["conversation_id"]}`).prependTo('#contact_list');
    $(`.c_filler_${data["conversation_id"]}`).find('.preview').text(data['message_body']);
    if (data['sender_id'] == $('#id').val()){
      $('#message_body').val("");
      $('#message_body').focus();
      scroll_to_top();
    }else{
      if ($(`.messages`).find('li').last().position().top - $('.messages').height() > 300){
        $('.new-message-notification').css("opacity", "1");
        new_notification_button()
      }
      window.ion.sound.play("fart");
    }
  }else if (data['status'] == 401 || data['status'] == 400){
    alert(data['message']);
  }else if (data['status'] == 203){
    $('#message_body').val("");
    alert(data['message']);
  }
  else{
    alert("ayg samok");
  }
}

function create_conversation(data){
  if (data['status'] == 200){
    $('#contacts > ul').prepend(data['conversation']);
    if (data['sender_id'] == $('#id').val()){
      $(`.c_filler_${data['conversation_id']}`).addClass('active')
      $('.messages > ul').addClass(`list_convo_${data['conversation_id']}`);
      $('.messages > ul').append(data['message']);
      $('.message-input > div.wrap').empty().append(data['form']);
      $('#message_body').val("");
      $('#message_body').focus();
    }
    else{
      window.ion.sound.play("fart");
    }
    scroll_to_top();
    reply_message();
  }else if (data['status'] == 401 || data['status'] == 400){
    alert(data['message']);
  }else if (data['status'] == 203){
    $('#message_body').val("");
    alert(data['message']);
  }
  else{
    alert("ayg samok");
  }
}
function new_notification_button(){
  detect_scroll($('.messages'));
  $('.new-message-notification').on('click',function(){
    scroll_to_top(function(){
      $('.new-message-notification').animate({opacity: 0}, 300);
      $('.messages').unbind();
    });
  })
}
function detect_scroll(selector){
  selector.on('scroll', function(){
    if (selector.find('li').last().position().top - selector.height() < 100){
      $('.new-message-notification').animate({opacity: 0}, 200);
      selector.unbind();
    }
  });
}
function scroll_to_top(callback = null){
  var selector = $('.messages');
  if (selector.length){
    selector.animate({scrollTop: (selector.prop("scrollHeight"))},500,callback)
  }
}

function reply_message(){
  $('.reply_button').on('click',function(){
    var message = $('#message_body').val();
    var convo_id = $('#convo_id').val();
    App.reply_message.reply_message(message,convo_id);
  })
  
}
export default App;