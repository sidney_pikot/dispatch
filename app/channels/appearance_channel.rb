class AppearanceChannel < ApplicationCable::Channel
  def subscribed
    stream_from "appearances_channel"
    # redis = Redis.new
    # puts current_user
    # # puts online
    # redis.set("user_#{current_user}_online", "1")
    ActionCable.server.broadcast "appearances_channel",
                                 user_id: current_user,
                                 online: true
  end
  def unsubscribed
    # redis = Redis.new
    # redis.del("user_#{current_user}_online")
    ActionCable.server.broadcast "appearances_channel",
                                 user_id: current_user,
                                 online: false
  end
end
