class ConversationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "conversation_#{current_user}_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
  def reply_message(data)
    User.transaction do
      user = User.find_by(id: current_user)
      conversation = user.mailbox.conversations.find_by(id: data['convo_id'])
      puts conversation
      if conversation.present?
          if !data['message'].present?
            ActionCable.server.broadcast "conversation_#{current_user}_channel",
                              action: "reply",
                              message: "Message can't be blank.",
                              status: 203
          else 
            message = user.reply_to_conversation(conversation,data['message']).message
            if message.present?
              @message = {
                id: message.id,
                body: message.body,
                conversation_id:message.conversation_id,
                attachment: message.attachment.file,
                created_at: message.created_at,
                sender: message.sender_id
              }
              # puts @message.inspect
              participants = conversation.becomes(AppConversation).app_participants
              participants.distinct.pluck(:user_id).each do |p|
                    ActionCable.server.broadcast "conversation_#{p}_channel",
                              action: "reply",
                              message: render_message(@message,p),
                              conversation_id: message.conversation_id,
                              message_body: message.body,
                              sender_id: message.sender_id,
                              status: 200
              end
            else
              ActionCable.server.broadcast "conversation_#{current_user}_channel",
                              action: "reply",
                              message: "Error sending message.",
                              status: 400
            end
          end
      else
        ActionCable.server.broadcast "conversation_#{current_user}_channel",
                              action: "reply",
                              message: "Unauthorized Access.",
                              status: 401
      end
    end
    rescue => e
      ActionCable.server.broadcast "conversation_#{current_user}_channel",
                              action: "reply",
                              message: e.inspect,
                              status: 401
  end

  def send_message(data)
    puts data
    User.transaction do
      receiver = User.where(id: JSON.parse(data['receiver_ids']))
      if receiver.present? 
        if data['message'].present?
          sender = User.find_by(id: current_user)
          puts receiver.inspect
          message =sender.send_message(receiver,data['message'],"nothing").message
          @receivers = []
          receiver.each do |r|
              @receivers.push(r.id)
          end
          @receivers.push(current_user)
          conversation = message.conversation
          conversation.becomes(AppConversation).create_participants(@receivers)
          participants = conversation.becomes(AppConversation).app_participants
          # @conversation = {
          #   id: conversation.id,
          #   last_message: conversation.last_message.body,
          #   participants: participants.map(&:user).select{|p|p.id != current_user.to_i}
          # }
          @message = {
            id: message.id,
            body: message.body,
            conversation_id:message.conversation_id,
            attachment: message.attachment.file,
            created_at: message.created_at,
            sender: message.sender_id
            }
          puts @conversation
          participants.distinct.map(&:user_id).uniq.each do |p|
            ActionCable.server.broadcast "conversation_#{p}_channel",
                              action: "create_conversation",
                              conversation: render_conversation(conversation,participants,p,message.sender_id,message.body),
                              message: render_message(@message,p),
                              sender_id: message.sender_id,
                              form: render_reply_form(message.conversation_id),
                              conversation_id: message.conversation_id,
                              status: 200
          end
        else
          ActionCable.server.broadcast "conversation_#{current_user}_channel",
                              action: "create_conversation",
                              message: "Message can't be blank.",
                              status: 203
        end
      else

      end
      result = {message: @message, status: 200}
    end
    rescue => exception
      puts exception
      result = {message: "Your message was not set because of an error.", status: 300}
  end

  def render_message(message,user_id)
    puts message
    ApplicationController.renderer.render(partial: 'ws/components/body/message', locals: {message: message,user_id: user_id})
  end
  def render_conversation(conv,participants,user_id,sender_id,last_message)
    ApplicationController.renderer.render(partial: 'ws/components/side_panel/conversation', locals: {conversation: conv,participants: participants.map(&:user),user_id: user_id,sender_id: sender_id,last_message: last_message})
  end
  def render_reply_form(conversation_id)
    ApplicationController.renderer.render(partial: 'ws/components/forms/reply_form', locals: {convo_id: conversation_id})
  end
end
