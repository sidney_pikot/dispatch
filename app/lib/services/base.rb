module Services
    module Base
      def self.included(base)
        base.extend ClassMethods
      end

      module ClassMethods
        def invoke(*args, &block)
          @instance ||= self.new
          @instance.invoke *args, &block
        end
      end
    end
end