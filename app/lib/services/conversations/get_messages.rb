class Services::Conversations::GetMessages
    include Services::Base
    include ApplicationHelper
    def invoke conversation_id, user
        User.transaction do 
            conversation = Mailboxer::Conversation.find_by(id: conversation_id)
            if conversation.present?
                participants = conversation.becomes(AppConversation).app_participants
                @messages = []
                receipts = conversation.receipts_for(user).not_trash.sort_by &:created_at
                receipts.each do |r|
                    @messages.push(format_message(r.message))
                end
                result = {messages: @messages, participants: participants, status: 200}
            else
                result = {messages: [], participants: [], status: 200}
            end
        end
        rescue => e
            result = {message: "An error occured while retrieving data.",status: 300}
    end

end