  class Services::Conversations::GetConversations
    include ApplicationHelper
    include Services::Base
    def invoke(user)
      User.transaction do
        conversations = user.conversations
        @conversations = []
        conversations.each do |c|
          @conversations.push(format_conversation(c,user.id))
        end
        result = {conversations: @conversations, status: 200}
      end
      rescue => exception
         result = {message: "An error occured while retrieving data.", status: 300}
    end
end