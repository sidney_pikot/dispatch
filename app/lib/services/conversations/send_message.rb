class Services::Conversations::SendMessage
    include Services::Base
    
    def invoke params, user
        User.transaction do
                receiver = User.where(id: JSON.parse(params[:id]))
                @message = user.send_message(receiver,params[:body],"nothing").message
                @receivers = []
                receiver.each do |r|
                    @receivers.push(r.id)
                end
                @receivers.push(user.id)
                @message.conversation.becomes(AppConversation).create_participants(@receivers)
                result = {message: @message, status: 200}
        end
        rescue => exception
            result = {message: "Your message was not set because of an error.", status: 300}
    end
end