class Services::Conversations::ReplyToConversation
    include Services::Base
    
    def invoke params, user
        User.transaction do
            conversation = user.mailbox.conversations.find_by(id: params[:id])
            if conversation.present?
                @message = user.reply_to_conversation(conversation,params[:body]).message
                if @message.present?
                    result = {message: @message, status: 200}
                else
                    result = {message: "Message cant be blank", status: 300}
                end
            else
                result = {message: nil, status: 300}
            end
        end
    rescue => e
        result = {message: nil, status: 300}
    end
end