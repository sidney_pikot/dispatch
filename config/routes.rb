Rails.application.routes.draw do
  devise_for :users, controllers: { 
    sessions: 'ws/users/sessions',
    registrations: "ws/users/registrations",
    omniauth_callbacks: 'ws/users/omniauth_callbacks'
   }
   mount ActionCable.server => '/cable'
  namespace :ws, path: "" do
    get '/conversations',                    to: 'conversations#home'
    namespace :users,path: 'user' do
      devise_scope :user do
        post 'sign_in',                     to: 'sessions#create'
        get 'sign_up',                      to: 'registrations#new'
        post 'register',                    to: 'registrations#create'
        delete 'sign_out',                  to: 'sessions#destroy'
        get 'fb_omniauth',                  to: 'omniauth_callbacks#facebook'
        get 'google_omniauth',              to: 'omniauth_callbacks#google_oauth2'

      end
    end
    
    get 'privacy-policy',                   to: 'static_pages#privacy_policy'
  end
  namespace :ajax do
    get 'conversations/home',             to: 'conversations#home'
    get 'conversations/get_conversation',     to: 'conversations#get_conversation'
    post 'conversations/send_message',    to: 'conversations#send_message'
    post 'conversations/reply',            to: 'conversations#reply'
    get 'conversations/search_contacts',   to: 'conversations#search_contacts'
    get 'conversations/get_contact',      to: 'conversations#get_contact'
    resources :users, only: [:index]
  end

  # root to: "ws/conversations#home", as: '/conversation'
  root :to => redirect('/conversations')
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
