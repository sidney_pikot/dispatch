class CreateAppParticipants < ActiveRecord::Migration[6.0]
  def change
    create_table :app_participants do |t|
      t.belongs_to :app_conversation
      t.belongs_to :user
      t.timestamps
    end
  end
end
