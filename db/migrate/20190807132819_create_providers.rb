class CreateProviders < ActiveRecord::Migration[6.0]
  def change
    create_table :providers do |t|
      t.string :uid
      t.string :provider
      t.string :link
      t.belongs_to :user
      t.timestamps
    end
    remove_column :users, :uid, :string
    remove_column :users, :provider, :string
  end
end
